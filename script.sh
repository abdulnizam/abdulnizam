#!/bin/bash

# Define container name
CONTAINER_NAME="abdulnizam"

# Find container ID by name
CONTAINER_ID=$(docker ps -q --filter "name=$CONTAINER_NAME")

# Check if the container is found
if [ -n "$CONTAINER_ID" ]; then
  echo "Stopping and removing container with name $CONTAINER_NAME..."
  docker stop $CONTAINER_ID
  docker rm $CONTAINER_ID
else
  echo "No containers found with name $CONTAINER_NAME."
fi

# Remove all Docker images
echo "Removing all Docker images..."
docker rmi $(docker images -q) -f

# Prune unused volumes
echo "Pruning unused Docker volumes..."
docker volume prune -f

# Run the new container
echo "Running new container..."
DOCKER_IMAGE="$1"
DOCKER_IMAGE_TAG="$2"

docker run -d --name $CONTAINER_NAME --network my-network  --memory="128m" --cpus="0.25" -p 4000:80 -p 4443:443 $DOCKER_IMAGE:$DOCKER_IMAGE_TAG
