const express = require("express");
const fs = require("fs");
const https = require("https");
const http = require("http");
const path = require("path");

const app = express();
const PORT = 3000;

app.use("/.well-known", express.static(path.join(__dirname, ".well-known")));

app.get("/", (req, res) => {
    res.send("Hello, Abdul Nizam!");
});

const sslOptions = {
    key: fs.readFileSync("/etc/letsencrypt/live/abdulnizam.com/privkey.pem"),
    cert: fs.readFileSync("/etc/letsencrypt/live/abdulnizam.com/fullchain.pem"),
};

http.createServer(app).listen(PORT, () => {
    console.log(`HTTP Server listening on port ${PORT}`);
});

https.createServer(sslOptions, app).listen(3443, () => {
    console.log(`HTTPS Server listening on port 3443`);
});
