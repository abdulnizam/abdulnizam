const express = require("express");
const app = express();

const PORT = process.env.PORT || 3000;

app.get("/", (req, res) => {
    res.redirect("https://www.linkedin.com/in/abdulnizam/");
    // res.send("Hello, Abdul Nizam!");
});

app.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}`);
});
